package EventosProject;

import Interfaces.EventoConfigurable;

public class EventoConfigurableImpl1 implements EventoConfigurable {
	
	@Override
	public String getEventoId() {
		return "1";
	}

	@Override
	public String getUsuarioId() {
		return "1";
	}

	@Override
	public String getUsuarioNickname() {
		return "HeumaX";
	}

	@Override
	public String getTipoPlataforma() {
		return "Twitter";
	}

	@Override
	public String getDivisaBase() {
		return "USD";
	}

	@Override
	public String getDivisaComparable() {
		return "ARS";
	}

	@Override
	public String getTipoEvento() {
		return "objetivo";
	}

	@Override
	public Double getValor() {
		return 83.00;
	}
}
