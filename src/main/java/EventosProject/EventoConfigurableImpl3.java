package EventosProject;

import Interfaces.EventoConfigurable;

public class EventoConfigurableImpl3 implements EventoConfigurable {

	@Override
	public String getEventoId() {
		return "3";
	}

	@Override
	public String getUsuarioId() {
		return "3";
	}

	@Override
	public String getUsuarioNickname() {
		return "Pepito";
	}

	@Override
	public String getTipoPlataforma() {
		return "Twitter";
	}

	@Override
	public String getDivisaBase() {
		return "BRL";
	}

	@Override
	public String getDivisaComparable() {
		return "ARS";
	}

	@Override
	public String getTipoEvento() {
		return "porcentual";
	}

	@Override
	public Double getValor() {
		return 0.05;
	}

}
