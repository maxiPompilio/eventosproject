package EventosProject;

import Interfaces.EventoConfigurable;

public class EventoConfigurableImpl2 implements EventoConfigurable {
	
	@Override
	public String getEventoId() {
		return "2";
	}

	@Override
	public String getUsuarioId() {
		return "2";
	}

	@Override
	public String getUsuarioNickname() {
		return "Kei";
	}

	@Override
	public String getTipoPlataforma() {
		return "Twitter";
	}

	@Override
	public String getDivisaBase() {
		return "EUR";
	}

	@Override
	public String getDivisaComparable() {
		return "ARS";
	}

	@Override
	public String getTipoEvento() {
		return "fijo";
	}

	@Override
	public Double getValor() {
		return 1.00;
	}

}
